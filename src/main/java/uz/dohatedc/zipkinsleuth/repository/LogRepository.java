package uz.dohatedc.zipkinsleuth.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import uz.dohatedc.zipkinsleuth.entity.Log;

public interface LogRepository extends MongoRepository<Log,String> {
}
