package uz.dohatedc.zipkinsleuth.controller;


import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import uz.dohatedc.zipkinsleuth.service.LogService;

import java.nio.file.Watchable;

@RestController
@RequestMapping("/api/v1/service_one")
@Slf4j
public class SleuthRestController {


    private final RestTemplate restTemplate;
    private final LogService logService;

    @Autowired
    public SleuthRestController(RestTemplate restTemplate, LogService logService) {
        this.restTemplate = restTemplate;
        this.logService = logService;
    }

    @GetMapping
    public String serviceOne() {
        String data = "service one";
        logService.save();
        return data;

    }

    int retryCount = 1;

    @GetMapping("/call")
    @CircuitBreaker(name = "service_two", fallbackMethod = "serviceTwoFallBack")
    @Retry(name = "service_two")
    @RateLimiter(name = "service_two")
    @Bulkhead(name = "service_two")
    public String callServiceTwo() {
        String data = "service one";
        logService.save();
        RateLimiter re;
        log.error("Retry " + (retryCount++) + " times");
        String res = restTemplate.getForObject("http://localhost:8085/api/v1/service_two", String.class);
        return data + " | " + res;
    }

    public String serviceTwoFallBack(Exception e) {
        return "This is a fallback method for service_two";
    }


}
