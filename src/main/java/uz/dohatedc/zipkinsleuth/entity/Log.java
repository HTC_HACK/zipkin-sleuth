package uz.dohatedc.zipkinsleuth.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Document("logs")
public class Log {

    @Id
    private String id;

    private String spanId;

    private String traceId;



}
